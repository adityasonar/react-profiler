import React, { Profiler } from 'react';
import './App.css';
import SampleComponent from './sample';

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      propText: 'Sample text'
    }
  }

  componentDidMount() {
    this.setState({
      propText: 'Changed text',
    })
  }

  showCost = (id) => (
    <div>{id}</div>
  );

  render() {
    return (
      <div className="App">
        <Profiler id='SampleComponent' onRender={this.showCost(id)} >
          <SampleComponent propText={this.state.propText} />
        </Profiler>
      </div>
    )
  }
}
