import React from 'react';

export default function SampleComponent(
    propText,
){
    return(
        <div>
            {propText}
        </div>
    )
}